module.exports = {
  client: {
    service: {
      name: 'local-schema',
      localSchemaFile: './src/schema.graphql',
    },
  },
};